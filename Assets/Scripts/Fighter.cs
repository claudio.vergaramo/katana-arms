using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Animator))]

public class Fighter : MonoBehaviour
{
    protected static Vector2 LimitsY = new Vector2(-7.17f, 2.17f);

    [SerializeField]
    protected float verticalSpeed;
    [SerializeField]
    protected float horizontalSpeed;

    protected Rigidbody2D r2D;
    protected SpriteRenderer spriteRenderer;
    protected Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        r2D = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
    }

    protected Vector2 control;
    // Update is called once per frame
    void Update()
    {
        
        

    }
}
