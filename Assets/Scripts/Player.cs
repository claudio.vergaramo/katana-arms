using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Player : Fighter
{
    void Update()
    {
        control = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        if(Input.GetKeyDown(KeyCode.Z)) //Con Z, golpeas
        {
            animator.SetTrigger("SendPunch");
        }

        //animator.SetBool("IsGuard", Input.GetKey(KeyCode.X)); // Con X te defiendes

        if(!animator.GetCurrentAnimatorStateInfo(0).IsName("Punch")
        && !animator.GetCurrentAnimatorStateInfo(0).IsName("GetPunch")
        && !animator.GetCurrentAnimatorStateInfo(0).IsName("Punch"))
        {
            r2D.velocity = new Vector2(control.x * horizontalSpeed, control.y * verticalSpeed);
            //animator.SetBool("IsWalking", control.magnitude != 0);
            transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, LimitsY.x, LimitsY.y), transform.position.z);
        }
        else 
        {
            r2D.velocity = Vector3.zero;
        }

        if(control.x != 0){
            spriteRenderer.flipX = control.x < 0;
        }
        

    }
}
